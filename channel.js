// No javascript as funções podem ser tratadas como classes e futuramente instanciadas como objetos
function Channel ( name ) { // O canal recebe um parâmetro com seu nome na sua criação

   // Crie os atributos necessários
   this.name = name; // O nome recebido na instanciação da classe é inserido num atributo
   this.users = [ ]; // Um array de membros do canal para ser utilizado posteriormente

}  
  //metodo para adicionar usuario ao canal
  Channel.prototype.addClient = function (client) {
      this.users.push(client);
      return 1;
  }; 

  //metodo para procurar usuario no canal
  Channel.prototype.findIndex = function (clientName) {
    var i;
    for(i = 0; i < this.users.length; i++) {
        if (this.users[i]["name"] === clientName) return i;
    }
    return -1;
  };

  //metodo para remover usuario do canal
  Channel.prototype.removeClient = function (client) {
      var pos;
      pos = this.findIndex(client["name"]);
      if(pos > -1){
        this.users.splice(pos, 1);
        return 1;
      }
      else return -1;
  }; 
   //metodo para listar clients no canal
  Channel.prototype.listClients = function () {
    var clients;
    var i;
    clients = [];
    for(i = 0; i < this.users.length; i++) {
      clients.push(this.users[i]["nick"]);
    }
      return clients;
  }; 

  //metodo para listar clients no canal
  Channel.prototype.listClientsSocket = function () {
    return this.users;
  }; 

// O segredo aqui é utilizar a função Module.exports para que possa utilizar o 
// Require depois para chamar a classe no servidor
module.exports = Channel;
