// Load the TCP Library
net = require('net');

//Load the Channel Class
Channel = require('./channel');

//array de canais
var channelsList  = [];

//array de canais
var channels = [];

// Nome do servidor
var serverName = "irc-server";

// Keep track of the chat clients
var clients = [];

//Mapa de nicknames
var nicks = {};

var today = new Date();
var dia = today.getDate();
var mes = today.getMonth()+1; //Janeiro é 0!
var ano = today.getFullYear();

// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  //cria uma lista dos canais dos quais o cliente participa
  channels[socket.name] = [];
  // Put this new client in the list
  clients.push(socket);



/*  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);*/

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    //broadcast(socket.name + "> " + data, socket);
    //Analisar as mensagens
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    if (nicks[socket.nick]) delete nicks[socket.nick];
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // escrever a msg no servidor
    //process.stdout.write(message)
  }

  function sendMessage(message, sender, channelIndex){
    var listC;
    var j;
    listC = channelsList[channelIndex].listClientsSocket();
    //console.log(channelsList[channelIndex].name  + " " + channelsList[channelIndex].listClients().toString() );
      for(j = 0; j < listC.length; j++){
        if (listC[j] === sender){
          continue;
        }  
        else{
          listC[j].write(message);
        }
      }
      listC = [];
    
    //escrever a msg no servidor
    //process.stdout.write(message);
  }
 
  function analisar(data) {
    var mensage = String(data).trim();
    var commands = tratarMensagem(mensage);
    var args = [];

    for(var i = 0; i < commands.length; i++) {
      args = commands[i];
      args[0] = args[0].toUpperCase();

      if(args[0] == "NICK") nick(args);
      else if(args[0] == "USER") user(args);
      else if(args[0] == "JOIN") join(args);
      else if(args[0] == "PRIVMSG") privmsg(args);
      else if(args[0] == "MODE") mode(args);
      else if(args[0] == "PART") part(args);
      else if(args[0] == "QUIT") quit(args);
      else {
        if(socket.nick && socket.username) 
          socket.write(":" + serverName + " 421 " + socket.nick + " " + args[0] + " :Unknown command\n");
      }
    }
  }

  function tratarMensagem(data){
    var aux = [];

    // Vamos supor que a entrada de texto foi um comando USER
    // data = "USER username 0 * :real name";

    // Trata o texto recebido pelo socket
    // Como os comandos podem chegar concatenados, precisamos separa-los primeiro
    var commands = data.toString().split(/\n/);

    // Executamos o tratamento para cada linha de comando recebida
    for (var i = 0; i < commands.length; i++) {

      var command = commands[i].toString().trim();

      // Separa os parâmetros do texto
      // args = "USER username 0 *";
      var args = command.split(/ :/)[0];

      // Caso haja uma mensagem no final ela é capturada
      // message = "real name";
      var message = command.split(args + " :")[1];

      // Separa os parâmetros transformando-os em array
      // args = ["USER", "username", 0, "*"];
      args = args.split(/ :/)[0].split(" ");

      // Se capturou alguma mensagem no final, insere no final do array de parâmetros
      // args = ["USER", "username", 0, "*", "real name"];
      if (message) args.push(message);

      aux[i] = args;
    }

    // Agora envie os parâmetros extraídos para executar a operação
    return aux;
  }

  function nick(args) {
    if(! args[1]) { // segundo argumento vazio
      socket.write(":" + serverName + " 431 * :No nickname given\n");
      return;
    }
    else if(! nicknameEhValido(args[1])) { // se o nickname for inválido
      socket.write(":" + serverName + " 432 * " + args[1] + " :Erroneous Nickname\n");
      return;
    }
    else if(nicks[args[1]]) { // se o nickname já estiver em uso
      socket.write(":" + serverName + " 433 * " + args[1] + " :Nickname is already in use\n");
      return;
    }
    else {
      if(socket.nick) {//Se já tem nickname
        var nicknameAntigo = socket.nick;

        //Remove o nick do mapa
        delete nicks[socket.nick];
      }
      //Inclui no mapa
      nicks[args[1]] = socket.name;

      //Registra no socket
      socket.nick = args[1];
      clients.splice(clients.indexOf(socket), 1);
      clients.push(socket);


      if(nicknameAntigo && socket.username) {
        socket.write(":" + nicknameAntigo + "!~" + socket.username + "@" + socket.remoteAddress + " NICK :" + socket.nick + "\n");
      }
      else if(socket.username) {
        socket.write(":" + serverName + " 001 " + socket.nick + " :Welcome to the Internet Relay Network " + socket.nick + "!" + socket.username + "@" + socket.remoteAddress + "\n");
        socket.write(":" + serverName + " 002 " + socket.nick + " :Your host is " + serverName + ", running version irc-server-1.0\n");
        socket.write(":" + serverName + " 003 " + socket.nick + " :This server was created " + dia + "/" + mes + "/" + ano + "\n");
      /*  Ainda é preciso definir os parâmetros da mensagem 004 para que possamos exibi-la
        socket.write(":" + serverName + " 004 " + socket.nick + <servername> <version> <available user modes> <available channel modes>"\n");
      */
      }
      //socket.write("OK : comando NICK executado com sucesso\n");
    }
    /* Erros não implementados:
          ERR_UNAVAILRESOURCE
          ERR_NICKCOLLISION
          ERR_RESTRICTED
    */
  }

  //Verifica se o nickname fornecido obedece à expressão regular que define um nickname
  function nicknameEhValido(nickname) {
    var ehValido = false;
    var i = 1;
    var letra = /[A-Z]/i;  //Expressão regular para uma letra de A à Z (case insensitive)
    var digito = /[0-9]/;  //Expressão regular para um dígito de 0 à 9
    var especial = /([\x5B-\x60]|[\x7B-\x7D])/;  //Expressão regular que busca em uma faixa hexadecimal pelos caracteres especiais: "[", "]", "\", "`", "_", "^", "{", "|", "}"

    ehValido = letra.test(nickname[0]) || especial.test(nickname[0]);
    while(ehValido && i < nickname.length) {
      ehValido = letra.test(nickname[i]) || digito.test(nickname[i]) || especial.test(nickname[i]) || /\-/.test(nickname[i]);
      i++;
    }

    return ehValido;
  }  

  function nickOrChannel(str){
    if(str.charAt(0) == "&" || str.charAt(0) == "#" 
    || str.charAt(0) == "+" || str.charAt(0) == "!"){//canal tem de comecar com 1 desses
      return 0; // 0 para canal
    }
    else return 1; // 1 para nick
  }

  function locSocket(nick){
	for ( var i = 0; i < clients.length; i++){
		if( clients[i].nick == nick){
			return clients[i];
		}	
	}
	return -1;
  }

  function user(args) {
    if(!socket.username) {
      if(!(args[1] && args[2] && args[3] && args[4])) {
        if(socket.nick) {
          socket.write(":" + serverName + " 461 " + socket.nick + " USER :Not enough parameters\n");
        }
        else {
          socket.write(":" + serverName + " 461 * USER :Not enough parameters\n");
        }
      }
      else if (isNaN(args[2])) {
       // socket.write("Erro: Modo deve ser numerico\n");  //Não existe mensagem irc nesse caso ?
        if(socket.nick) {
          socket.write(":" + serverName + " NOTICE " + socket.nick + " :*** Mode parameter should be a number\n");
        }
        else {
          socket.write(":" + serverName + " NOTICE * :*** Mode parameter should be a number\n");
        }
      }
      else {
        var umode = args[2] & 12;

        if(umode == 12) socket.usermode = "+wi";
        else if(umode == 8) socket.usermode = "+i";
        else if(umode == 4) socket.usermode = "+w";
        else socket.usermode = "+";

        socket.username = args[1]; 
        socket.fullname = args[4];

        //socket.write(socket.fullname + "\n");

        if(socket.nick) {
          socket.write(":" + serverName + " 001 " + socket.nick + " :Welcome to the Internet Relay Network " + socket.nick + "!" + socket.username + "@" + socket.remoteAddress + "\n");
          socket.write(":" + serverName + " 002 " + socket.nick + " :Your host is " + serverName + ", running version irc-server-1.0\n");
          socket.write(":" + serverName + " 003 " + socket.nick + " :This server was created " + dia + "/" + mes + "/" + ano + "\n");
        /*  Ainda é preciso definir os parâmetros da mensagem 004 para que possamos exibi-la
          socket.write(":" + serverName + " 004 " + socket.nick + <servername> <version> <available user modes> <available channel modes>"\n");
        */
        }
      }
    }
    else {
      socket.write(":server 462 " + socket.nick + " :Unauthorized command (already registered)\n");
    }
    /* Erro não implementado:
          ERR_ALREADYREGISTRED
    */
  }

  function channelExist(channelName){
    var i;
    for(i = 0; i < channelsList.length; i++) {
        if (channelsList[i].name === channelName) return i;
    }
    return -1;

  }

  function validateChannel(channelName){
    //IMPLEMENTAR    

  }

  function join(args) {
    var i;
    var newChannel;

     if(! args[1]) { // sem nome do canal
          socket.write(":" + serverName + " 461 * :Need more params\n");
          return;
      }
      else if(channelExist(args[1]) == -1) { // se o canal nao existe
          // sera preciso criar um novo canal e adiciona o cliente 
          newChannel = new Channel(args[1]);
          newChannel.addClient(socket);
          //adiciona canal na lista de canais
          channels[socket.name].push(args[1]);
          channelsList.push(newChannel);
          //mensagens ao usuario
          socket.write(":" + serverName + " Welcome to new channel " + args[1] + "\n");
          socket.write(":" + serverName + " Clients on channel: " + newChannel.listClients().toString() + "\n");
          //socket.write(":" + serverName + " Vc esta nos canais: " + channels[socket.name].toString() + "\n");
          return;
      }
      else if(channelExist(args[1]) != -1) { // se o canal ja existe
          // sera preciso somente adicionar o cliente ao canal
          i = channelExist(args[1]);
          channelsList[i].addClient(socket);
          channels[socket.name].push(args[1]);
          //mensagens ao usuario
          socket.write(":" + serverName + " Welcome to channel " + args[1] + "\n");
          socket.write(":" + serverName + " Clients on channel: " + channelsList[i].listClients().toString() + "\n");
          //socket.write(":" + serverName + " Vc esta nos canais: " + channels[socket.name].toString() + "\n");
          return;
      }
    
  }  

  function privmsg(args){
    var text;
    var mensagem;
    if(!args[1]){ // se nao existe destinatario
      socket.write(":" + serverName + "" + socket.nick + " ERR_NORECIPIENT \n");
      return;
    }
    else if(!args[2]){ // se nao existe mensagem
      socket.write(":" + serverName + "" + socket.nick + " ERR_NOTEXTTOSEND \n");
      return;
    }
    else if(nickOrChannel(args[1]) == 0){// no caso de canal
      if(channelExist(args[1]) != -1){// se o canal existe
        text = catArray(args);
        mensagem = socket.nick + ' on ' + args[1]+ ' > ' + text + '\n';
        sendMessage(mensagem, socket, channelExist(args[1]));

      }
      else{
        socket.write(":" + serverName + "" + socket.nick + "  ERR_CANNOTSENDTOCHAN \n");
      }
    }
    else{
	    var socketaux = locSocket(args[1]);
     	if (socketaux == -1){
		    socket.write(":" + serverName + "" + socket.nick + " ERR_NOSUCHNICK \n");
	    } 
      else {
    		text = catArray(args);
    		mensagem = socket.nick + ' on ' + args[1]+ ' > ' + text + '\n';
    		socketaux.write(mensagem);
    	}
	
    }
  }

  function mode(args) {
    if(args[1]) {
      if(args[1] === socket.nick) {
        if(args[2]) {
          var newModes = "";
          var add = true;
          var aux = args[2].toString().split("");  //Transforma cada char de args[2] em um elemento de um array

          if(/[^iwao\+\-]/i.test(args[2])) {
            socket.write(":" + serverName + " 501 " + socket.nick + " :Unknown MODE flag\n");
          }

          if(/a/i.test(args[2])) {
            socket.write(":" + serverName + " NOTICE " + socket.nick + " :*** You should use the AWAY command to toggle 'a' flag\n");
          }

          for(var i = 0; i < aux.length; i++) {
            if(aux[i] == "+") add = true;
            else if(aux[i] == "-") add = false;
            else if(/[iwao]/i.test(aux[i])) {
              if(add) newModes = newModes.concat(addMode(aux[i]));
              else newModes = newModes.concat(removeMode(aux[i]));
            }
          }

          if(newModes != "") {
            var addArray = ["+"];
            var rmArray = ["-"];
            var modeArray = newModes.split("");

            var i = 0
            while(i < modeArray.length) {
              if(modeArray[i] == "+") {
                addArray.push(modeArray[i+1]);
                i++;
              }
              else if(modeArray[i] == "-") {
                rmArray.push(modeArray[i+1]);
                i++;
              }
              i++;
            }  

            i = 0;

            while(i < addArray.length) {
              var pos = rmArray.indexOf(addArray[i]);

              if(pos > -1) {
                rmArray.splice(pos, 1);
                addArray.splice(i, 1);
              }
              else {
                i++;
              }
            }

            newModes = "";
            if(rmArray.length > 1) {
              for(i = 0; i < rmArray.length; i++) {
                newModes = newModes.concat(rmArray[i].toString());
              }
            }
            if(addArray.length > 1) {
              for(i = 0; i < addArray.length; i++) {
                newModes = newModes.concat(addArray[i].toString());
              }
            }

            socket.write(":" + socket.nick + " MODE " + socket.nick + " :" + newModes + "\n");
          }
        }
        else {
          socket.write(":" + serverName + " 221 " + socket.nick + " " + socket.usermode + "\n");
        }
      }
      else if(nicks[args[1]]) {
        socket.write(":" + serverName + " 502 " + socket.nick + " :Can't change mode for other users\n");
      }
      else if(channelExist(args[1]) > -1) {
        socket.write(":" + serverName + " NOTICE " + socket.nick + " :*** No modes available for channels\n");
    /* ------------------------------------
       ------------------------------------
       IMPLEMENTAR MODE PARA CANAIS
       ------------------------------------
       ------------------------------------ */
      }
      else {
        socket.write(":" + serverName + " 403 " + socket.nick + " " + args[1] + " :No such channel\n");
      }
    }
    else {
      socket.write(":" + serverName + " 461 " + socket.nick + " MODE :Not enough parameters\n");
    }
  }

  function addMode(newMode) {
    var strMode = newMode.toString().toLowerCase();
    var regex = new RegExp(strMode);

    if(regex.test(socket.usermode)) return "";
    else if(strMode == "a") return "";
    else if(strMode == "i" || strMode == "w") {
      socket.usermode = socket.usermode.concat(strMode);
      return "+".concat(strMode);
    }
    else if(strMode == "o") {
      socket.write(":" + serverName + " NOTICE " + socket.nick + " :*** You should use the OPER command to obtain operator privileges\n");
      return "";
    }

    return "";
  }

  function removeMode(newMode) {
    var strMode = newMode.toString().toLowerCase();
    var regex = new RegExp(strMode);

    if(regex.test(socket.usermode) && strMode != "a") {
      var index = socket.usermode.indexOf(strMode);
      var aux = socket.usermode.slice(0, index);
      var aux2 = socket.usermode.slice(index+1);

      socket.usermode = aux.concat(aux2);
      return "-".concat(strMode);
    }

    return "";
  }

  //funcao para concatenar os elementos de arg e formar a msg a ser mandada no chat
  function catArray(args){
    var text = "";
    var i;
    for(i = 2; i < args.length; i++){
      text = text + " " + args[i];
    }
    return text;
    text = "";
  }

  function quit( args ) {
	  socket.end(args[1]);
  }

  function part(args) {
    if(!args[1]) {
      socket.write(":" + serverName + " 461 " + socket.nick + " PART :Not enough parameters\n");
    }
    else {
      // Cria um array com os canais que o usuário deseja deixar
      var chans = args[1].split(",");

      for(var i = 0; i < chans.length; i++) {
        var posChannel = channelExist(chans[i]);

        if(posChannel == -1) {  // Se o canal não existe
          socket.write(":" + serverName + " 403 " + socket.nick + " " + chans[i] + " :No such channel\n");
        }
        else { // Se o canal existe
          // Verifica se o cliente está no canal
          var posClient = channelsList[posChannel].findIndex(socket.name);

          if(posClient == -1) {  // Se o cliente não estiver
            socket.write(":" + serverName + " 442 " + socket.nick + " " + chans[i] + " :You're not on that channel\n");
          }
          else {  // Se o cliente estiver no canal
            if(args[2]) {
              sendMessage(":" + socket.nick + "!~" + socket.username + "@" + socket.remoteAddress + " PART " + chans[i] + " :" + args[2] + "\n", socket, posChannel);
              socket.write(":" + socket.nick + "!~" + socket.username + "@" + socket.remoteAddress + " PART " + chans[i] + " :" + args[2] + "\n");
            }
            else {
              sendMessage(":" + socket.nick + "!~" + socket.username + "@" + socket.remoteAddress + " PART " + chans[i] + "\n", socket, posChannel);
              socket.write(":" + socket.nick + "!~" + socket.username + "@" + socket.remoteAddress + " PART " + chans[i] + "\n");
            }
            channelsList[posChannel].removeClient(socket);
          }
        }
      }
    }
  }


}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");
